package Calc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class calc {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            String input = reader.readLine();
            Pattern pNum = Pattern.compile("^\\-\\d|^\\-|\\((\\-\\d)\\)|\\((\\-\\d)|[0-9]*[.,]?[0-9]|[+\\-*/()]");
            List<String> Arr = new ArrayList<>();
            List<String> Substring;

            try{
                int leftBracket = 0;
                int rightBracket = 0;
                for (int i =0; i<input.length(); i++){
                    if(input.charAt(i)=='('){
                        leftBracket++;
                    }else if(input.charAt(i)==')'){
                        rightBracket++;
                    }
                }
                if(leftBracket!=rightBracket){
                    throw new Exception("Пропущена скобка(и)");
                }
                if(!input.matches("[\\d+\\-*/().]+")){
                    throw new Exception("Присутствуют неверные символы");
                }

                Matcher mNum = pNum.matcher(input);

                while (mNum.find()){
                    if(mNum.group(1) != null){
                        Arr.add(mNum.group(1));
                    }else if(mNum.group(2) != null){
                        Arr.add("(");
                        Arr.add(mNum.group(2));
                    }else{
                        Arr.add(mNum.group());
                    }
                }

                Integer first =null;
                Integer tip =null;

                for (int i = 0; i < Arr.size(); i++) {
                    if(Arr.get(i).equals("(")){
                        first = i;
                    }
                    if(Arr.get(i).equals(")")){
                        tip = i+1;
                    }
                    if(first !=null && tip !=null) {
                        Substring = Arr.subList(first, tip);
                        Substring = Estimation(Substring,"*","/");
                        Substring = Estimation(Substring,"+","-");
                        String substringResult = Substring.get(1);
                        Substring.clear();
                        Arr.add(first,substringResult);
                        i = 0;
                        first = null;
                        tip = null;
                    }
                }
                Arr = Estimation(Arr,"*","/");
                Arr = Estimation(Arr,"+","-");

                if(Arr.size()!=1){
                    throw new Exception("Пропущен знак");
                }
                for(String sum:Arr){
                    if((sum == "Infinity") || (sum == "-Infinity")){
                        throw new Exception("Делить на ноль нельзя");
                    }
                        System.out.println(sum);
                    }

            } catch (NumberFormatException e){
                System.out.println("Неверное выражение");
                System.exit(0);
            } catch (Exception ex){
                System.out.println(ex.getMessage());
                System.exit(0);
            }
        }
    }


    private static Double oper(Double leftNumber, Double rightNumber, String oper){
        Double result = 0.0;
        switch (oper){
            case ("+"):
                result = leftNumber+rightNumber;
                break;
            case ("-"):
                result = leftNumber-rightNumber;
                break;
            case ("*"):
                result = leftNumber*rightNumber;
                break;
            case ("/"):
                result = leftNumber/rightNumber;
                break;
        }
        return result;
    }

    private static List Estimation(List<String> list, String signOne, String signTwo){
        try {
            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).equals(signOne) || list.get(j).equals(signTwo)) {
                    String leftNumber = list.get(j - 1);
                    String rightNumber = list.get(j + 1);
                    String action = list.get(j);
                    Double result = oper(Double.parseDouble(leftNumber), Double.parseDouble(rightNumber), action);
                    for (int n = 0; n < 3; n++) {
                        list.remove(j - 1);
                    }
                    if (list.isEmpty()) {
                        list.add(result.toString());
                    } else {
                        list.add(j - 1, result.toString());
                    }
                    j = j - 2;
                }
            }
        }catch (IndexOutOfBoundsException ex){
            System.out.println("Пропущен оператор или число, вычисление невозможно");
            System.exit(0);
        }
        return list;
    }
}
